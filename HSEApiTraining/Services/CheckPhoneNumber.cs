﻿using System.Linq;

namespace HSEApiTraining
{
    public class Checker
    {
        /// <summary>
        /// Проверяет номер телефона number на правильность
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string CheckPhoneNumber(string number)
        {
            if (number.Length < 2)
                return "Таких нумберов не должно быть по логике.";
            if (!number.StartsWith('+'))
                return "Ну ты шо, номер же с + начинается";
            if (number.Length > 13)
                return "Ну блеен, номер ж не больше 13 цифр";
            // Я это условие из сообщений в тг взял, и вообще по логике номера такие должны быть
            if (!number.Substring(1).All(x => char.IsDigit(x)))
                return "Так цифры же просили, алееееее";
            return null;
        }
    }
}
