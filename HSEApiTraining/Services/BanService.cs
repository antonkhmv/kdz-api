﻿using HSEApiTraining.Ban;
using HSEApiTraining.Repositories;
using System.Collections.Generic;

namespace HSEApiTraining
{
    //       БУЗИНЕСС-ЛОДЖИК ЛЕУЕР
    public interface IBanService
    {
        (IEnumerable<Phone> Phones, string Error) GetAllPhones();
        string DeletePhone(int id);
        string DeleteAllPhones();
        string AddPhone(string number);
    }

    public class BanService : IBanService
    {
        private IBanRepository _BanRepository;

        public BanService(IBanRepository BanRepository)
            => _BanRepository = BanRepository;

        public string AddPhone(string number)
        {
            var message = Checker.CheckPhoneNumber(number);
            if (message != null)
            {
                return message;
            }
            return _BanRepository.AddPhone(number);
        }

        public string DeleteAllPhones()
            => _BanRepository.DeleteAllPhones();

        public string DeletePhone(int id)
            => _BanRepository.DeletePhone(id);

        public (IEnumerable<Phone> Phones, string Error) GetAllPhones()
            => _BanRepository.GetAllPhones();
    }
}