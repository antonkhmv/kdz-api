﻿using System;


namespace HSEApiTraining
{
    //       БУЗИНЕСС-ЛОДЖИК ЛЕУЕР
    public interface IDummyService
    {
        string DummyGenerator(int number);
    }

    public class DummyService : IDummyService
    {

        private readonly Random rand;

        public DummyService()
        {
            rand = new Random();
        }
        public string DummyGenerator(int number)
        {
            if (number < 0)
            {
                return "Error: number should be positive.";
            }
            // На всякий случай.
            try
            {
                return $"Random (0, {number}): {rand.Next(number)}";
            }
            catch
            {
                return "Unknown error.";
            }
        }
    }
}
