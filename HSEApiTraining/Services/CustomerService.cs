﻿using HSEApiTraining.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HSEApiTraining
{
    //       БУЗИНЕСС-ЛОДЖИК ЛЕУЕР
    public interface ICustomerService
    {
        (IEnumerable<Customer> Customers, string Error) GetCustomers(int count);
        (IEnumerable<Customer> Customers, string Error) GetAllCustomers();
        (IEnumerable<Customer> Customers, string Error) GetBannedCustomers();
        (IEnumerable<Customer> Customers, string Error) GetNotBannedCustomers();
        string AddCustomer(AddCustomerRequest request);
        string UpdateCustomer(int id, UpdateCustomerRequest request);
        string DeleteCustomer(int id);
        string GenerateRandomCustomer(GenerateRandomCustomersRequest request);
        string DeleteAllCustomers();
        (IEnumerable<Customer> Customers, string Error) SearchBySurname(string searchTerm);
        (IEnumerable<Customer> Customers, string Error) SearchByName(string searchTerm);

    }

    public class CustomerService : ICustomerService
    {

        private ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository) 
            => _customerRepository = customerRepository;

        public (IEnumerable<Customer> Customers, string Error) GetCustomers(int count)
            => _customerRepository.GetCustomers(count);

        public (IEnumerable<Customer> Customers, string Error) GetAllCustomers()
            => _customerRepository.GetAllCustomers();

        public string AddCustomer(AddCustomerRequest request)
        {
            var message = Checker.CheckPhoneNumber(request.PhoneNumber);
            if (message != null)
            {
                return message;
            }
            return _customerRepository.AddCustomer(request);
        }
        public string DeleteCustomer(int id)
            => _customerRepository.DeleteCustomer(id);

        public string DeleteAllCustomers()
            => _customerRepository.DeleteAllCustomers();

        public string UpdateCustomer(int id, UpdateCustomerRequest request)
        {
            var message = Checker.CheckPhoneNumber(request.PhoneNumber);
            if (message != null)
            {
                return message;
            }
            return _customerRepository.UpdateCustomer(id, request);
        }

        // рандомайзер
        static Random rand = new Random();

        /// <summary>
        /// Генерирует случайную стрингу из цифр длины length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GenerateRandomNumber(int length)
        {

            return string.Join("",
                Enumerable.Range(0, length)
                    .Select(i => (char)('0' + rand.Next(10))));
        }

        public string GenerateRandomCustomer(GenerateRandomCustomersRequest request)
        {
            if (request.Count < 0)
            {
                return "А может быть своей головой немножечко подумаешь и" +
                    " придешь к умозаключению, что невозможно добавить" +
                    " отрицательное число вещей к множеству, а?";
            }

            // Список имен и фамилий
            var names = new List<(string FirstName, string LastName)>() {
                ("Donald", "Trump"),
                ("Vladimir", "Pupkin"),
                ("Elon", "Musk"),
                ("Andrey", "Stepanov"),
                ("Ivan", "Lotoff"),
                ("Anton", "Khomyakoff"),
                ("Dima", "Petroff"),
                ("Sergey", "Ivanoff"),
                ("Vasilii", "Utkin"),
                ("Stepan", "Manchikoff"),
            };

            // Коды стран видимо
            var codes = new List<string>()
            {
                "+7", "+0", "+380", "+1"
            };

            // Длина номера телефона
            const int length = 8;

            var customers = new List<AddCustomerRequest>();

            foreach (var i in Enumerable.Range(0, request.Count))
            {
                var name = names[rand.Next(names.Count)];
                var code = codes[rand.Next(codes.Count)];
                var number = GenerateRandomNumber(length);

                customers.Add(new AddCustomerRequest()
                {
                    Name = name.FirstName,
                    Surname = name.LastName,
                    PhoneNumber = code + number
                });
            }

            return _customerRepository.GenerateRandomCustomers(customers);
        }

        public (IEnumerable<Customer> Customers, string Error) SearchByName(string searchTerm)
            => _customerRepository.SearchByName(searchTerm);

        public (IEnumerable<Customer> Customers, string Error) SearchBySurname(string searchTerm)
            => _customerRepository.SearchBySurname(searchTerm);

        public (IEnumerable<Customer> Customers, string Error) GetBannedCustomers()
            => _customerRepository.GetBannedCustomers();

        public (IEnumerable<Customer> Customers, string Error) GetNotBannedCustomers()
            => _customerRepository.GetNotBannedCustomers();
    }
}
