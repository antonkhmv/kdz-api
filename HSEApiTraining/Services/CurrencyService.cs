﻿using HSEApiTraining.Models.Currency;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace HSEApiTraining
{
    public interface ICurrencyService
    {
        Task<CurrecnyResponse> GetCurrencyResponse(CurrencyRequest currecnyRequest);
    }
    class CurrencyService : ICurrencyService
    {
        //       БУЗИНЕСС-ЛОДЖИК ЛЕУЕР
        public async Task<CurrecnyResponse> GetCurrencyResponse(CurrencyRequest currecnyRequest)
        {
            CurrecnyResponse MyCurrencyResponse = new CurrecnyResponse();
            try
            {
                if (currecnyRequest.DateStart == null)
                {
                    currecnyRequest.DateStart = DateTime.Now.Date;
                }

                if (currecnyRequest.DateEnd == null)
                {
                    currecnyRequest.DateEnd = currecnyRequest.DateStart;
                }

                if (currecnyRequest.DateEnd < currecnyRequest.DateStart)
                {
                    throw new ArgumentException("Start date shouldn't be after the end date.");
                }

                DateTime dateTime = (DateTime)currecnyRequest.DateStart;

                do
                {
                    // Метод в авейте не ловит эксепшены к сожалению :(.
                    string error = null;
                    
                    // Ответ от ratesapi.io
                    var response = await Task.Run(() =>
                    {
                        try
                        {
                            return WebRequestByDate(dateTime, currecnyRequest.Symbol);
                        }
                        // Плохой ответ от сервра.
                        catch(WebException)
                        {
                            error = "Unknown currency.";
                        }
                        catch (Exception ex)
                        {
                            error = ex.Message;
                        }
                        return null;
                    });

                    if (error != null)
                    {
                        throw new Exception(error);
                    }
                    // Десеарилизуем словарик
                    CurrencyJsonToSharp currency = JsonConvert.DeserializeObject<CurrencyJsonToSharp>(response);
                    double CurValue;
                    currency.rates.TryGetValue("RUB", out CurValue);
                    MyCurrencyResponse.CurrencyRate.Add(CurValue);
                    dateTime = dateTime.AddDays(1);
                }
                while (dateTime <= currecnyRequest.DateEnd);
            }
            catch (Exception ex)
            {
                MyCurrencyResponse.Error = ex.Message;
            }
            return MyCurrencyResponse;
        }

        // Перепиши это, использву ассинки
        /// <summary>
        /// Получает данные с сервера по заданной дате
        /// </summary>
        /// <param name="RequestDate">Важно, что формат yyyy-MM-dd</param>
        /// <param name="symbol">имя валюты</param>
        /// <returns></returns>
        public string WebRequestByDate(DateTime RequestDate, string symbol)
        {
            if (RequestDate.TimeOfDay != new TimeSpan(0))
            {
                throw new ArgumentException("Wrong date format. (should be yyyy-mm-dd).");
            }

            string Link = "https://api.ratesapi.io/api/" + RequestDate.ToString("yyyy-MM-dd") + "?base=" + symbol;
            WebRequest webRequest = WebRequest.Create(Link);
            WebResponse response = webRequest.GetResponse();
            using (StreamReader sr = new StreamReader(response.GetResponseStream()))
            {
                return sr.ReadToEnd();
            }

        }

    }
    /// <summary>
    /// Класс С# который в будущем будет десириализован из Json
    /// </summary>
    class CurrencyJsonToSharp
    {
        public string @base { get; set; }
        public Dictionary<string, double> rates { get; set; }
    }
}
