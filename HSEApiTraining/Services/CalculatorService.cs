﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace HSEApiTraining
{
    //       БУЗИНЕСС-ЛОДЖИК ЛЕУЕР
    public interface ICalculatorService
    {
        double CalculateExpression(string expression);
        IEnumerable<double> CalculateBatchExpressions(IEnumerable<string> expressions);
    }

    public class CalculatorService : ICalculatorService
    {
        public IEnumerable<double> CalculateBatchExpressions(IEnumerable<string> expressions)
        {
            if (expressions == null || expressions.Count() == 0)
            {
                throw new ArgumentNullException("No expressions.");
            }
            List<double> values = new List<double>();
            int index = 1;
            try
            {
                foreach (var expression in expressions)
                {
                    double result = 0;
                    result = CalculateExpression(expression);
                    values.Add(result);
                    index++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + $" (expression #{index}).", ex);
            }
            return values;
        }

        /// <summary>
        /// Отбрасывает все цифры после n-ного знака после разделителя.
        /// </summary>
        /// <param name="a">Число.</param>
        /// <param name="n">Количество отбрасываемых символов.</param>
        /// <returns></returns>
        public static double DropSignificantDigits(double a, int n)
        {
            a = Math.Truncate(a * Math.Pow(10, n));
            return a / Math.Pow(10, n);
        }

        public double CalculateExpression(string expression)
        {
            // expression всегда null если в строке только пробелы почему-то
            if (expression == null)
            {
                throw new FormatException("Empty expression.");
            }

            // Сплит строки по пробелам без пустых строк
            string[] input = expression.Split(' ', options: StringSplitOptions.RemoveEmptyEntries);

            // Если слишком много чисел - ошибка
            if (input.Length > 3)
            {
                throw new FormatException("Wrong format: too many literals.");
            }
            else if (input.Length == 3)
            {
                // Если 3 литерала, то второй из них - оператор.
                if (!"+/*%-".Contains(input[1][0]) || input[1].Length != 1)
                {
                    throw new FormatException("If the expression has 3 literals, the second one should be an operator.");
                }
            }
            else if (input.Length == 2)
            {
                // Если литерала 2, то в начале первого или конце второго должен быть оператор
                if (!"+/*%-".Contains(input[0][input[0].Length-1]) && 
                    !"+/*%-".Contains(input[1][0])) {
                    throw new FormatException("If the expression has 2 literals, there should be " +
                        "an operator in the beginning of the second one, or at the end of the first one.");
                }
            }
            char @operator = (char)0;

            // Склеиваем выражение обратно в одну строку без пробелов.
            expression = string.Join("", input);

            // Проверка на наличие операторов и сплит по ним
            // Минус последним, чтобы не перепутать отрицательные числа и операторы.
            foreach (var c in "+/*%-")
            {
                if (expression.Contains(c))
                {
                    input = expression.Split(c);
                    @operator = c;
                    break;
                }
            }

            if (@operator == 0)
            {
                throw new FormatException("No operator.");
            }
            if (input.Length > 2)
            {
                // Если более одного оператора (для минуса своя проверка из-за отрицательных чисел)
                if (@operator != '-')
                {
                    throw new FormatException("Wrong format: too many operators.");
                }
                // Минусов может быть не более 3, один - оператор, и два - в начале каждого из чисел.
                else if (input.Length <= 4)
                {
                    input = CheckExpression(input);

                    // Если проверкка на отрицательные числа не пройдена - ошибка.
                    if (input[0] == null)
                    {
                        throw new FormatException("Wrong format: too many operators.");
                    }
                }
            }
            // Проглатывается и точка, и запятая.
            input[0] = input[0].Replace(',', '.');
            input[1] = input[1].Replace(',', '.');
            double a, b;
            try
            {
                a = double.Parse(input[0], new CultureInfo("EN-en"));
                b = double.Parse(input[1], new CultureInfo("EN-en"));
            }
            catch
            {
                throw new FormatException("Not a number.");
            }

            if (int.MinValue > a || a > int.MaxValue || int.MinValue > b || b > int.MaxValue)
            {
                throw new FormatException($"The number is not in the interval [{int.MinValue}, {int.MaxValue}]");
            }

            if (b == 0)
            {
                throw new DivideByZeroException("Can't divide by zero.");
            }

            var operations = new Dictionary<char, Func<double, double, double>>()
            {
                {'+', (x,y) => x+y },
                {'-', (x,y) => x-y },
                {'*', (x,y) => x*y },
                {'/', (x,y) => x/y },
                {'%', (x,y) => x%y },
            };
            return DropSignificantDigits(operations[@operator](a, b), 4);
        }

        /// <summary>
        /// Проверяет явлеется ли проспличенное по знаку '-' выражение корректным.
        /// </summary>
        /// <param name="input">Массив строк - проспличенное по минусу выражение</param>
        /// <returns></returns>
        public static string[] CheckExpression(string[] input)
        {
            string[] output = new string[2];
            // Предпологаемое количество  минусов
            int amount = input.Length - 1;

            // Если минусов 3
            if (amount == 3)
            {
                // Если перед каждым числом стоит минус
                if (input[0] == "" || input[2] == "")
                {
                    // Записываем оба числа с минусами 
                    output[0] = "-" + input[1];
                    output[1] = "-" + input[3];
                }
            }
            // Если 2
            if (amount == 2)
            {
                // Если перед 1м числом стоит минус
                if (input[0] == "")
                {
                    // Записываем 1е с минусом, 2е - без
                    output[0] = "-" + input[1];
                    output[1] = input[2];
                }

                // Если перед 2м числом стоит минус
                if (input[1] == "")
                {
                    // Записываем 2е с минусом, 1е - без
                    output[0] = input[0];
                    output[1] = "-" + input[2];
                }
            }
            return output;
        }

    }
}
