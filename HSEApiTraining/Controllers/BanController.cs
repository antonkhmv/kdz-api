﻿using HSEApiTraining.Models.Ban;
using Microsoft.AspNetCore.Mvc;

namespace HSEApiTraining.Controllers
{
    //      ПРЕЗЕНТЕЙШОН ЛЕУЕР
    [Route("api/[controller]")]
    [ApiController]
    public class BanController : ControllerBase
    {
        private readonly IBanService _BanService;

        public BanController(IBanService BanService)
        {
            _BanService = BanService;
        }

        [HttpPost]
        public AddBanResponse AddBan([FromBody] AddBanRequest request)
        { 
            var result = _BanService.AddPhone(request.Phone);
            return new AddBanResponse
            {
                Error = result
            };
        }

        [HttpGet]
        public GetBannedPhonesResponse GetAllBannedPhones()
        {
            var result = _BanService.GetAllPhones();
            return new GetBannedPhonesResponse
            {
                Error = result.Error,
                Phones = result.Phones
            };
        }

        [HttpDelete("{id}")]
        public DeleteBanResponse DeleteNumber(int id)
        {
            var result = _BanService.DeletePhone(id);
            return new DeleteBanResponse
            {
                Error = result
            };
        }

        [HttpDelete("DeleteAll")]
        public DeleteBanResponse DeleteAllNumbers()
        {
            var result = _BanService.DeleteAllPhones();
            return new DeleteBanResponse
            {
                Error = result
            };
        }
    }
}
