﻿using HSEApiTraining.Models.Customer;
using Microsoft.AspNetCore.Mvc;

namespace HSEApiTraining.Controllers
{
    //      ПРЕЗЕНТЕЙШОН ЛЕУЕР
    [Route("api/[controller]")]
    [ApiController]
    public class  CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public GetCustomersResponse GetCustomers([FromQuery]int count)
        {
            var result = _customerService.GetCustomers(count);
            return new GetCustomersResponse
            {
                Customers = result.Customers,
                Error = result.Error
            };
        }

        [HttpGet("GetAll")]
        public GetCustomersResponse GetAllCustomers()
        {
            var result = _customerService.GetAllCustomers();
            return new GetCustomersResponse
            {
                Customers = result.Customers,
                Error = result.Error
            };
        }

        [HttpPost("GenerateRandomCustomers")]
        public GenerateRandomCustomersResponse GenerateRandomCustomers([FromBody] GenerateRandomCustomersRequest request)
        {
            return new GenerateRandomCustomersResponse
            {
                Error = _customerService.GenerateRandomCustomer(request)
            };
        }

        [HttpPost]
        public AddCustomerResponse AddCustomer([FromBody] AddCustomerRequest request)
        {
            return new AddCustomerResponse
            {
                Error = _customerService.AddCustomer(request)
            };
        }

        [HttpPut("{id}")]
        public UpdateCustomerResponse UpdateCustomer(int id, [FromBody] UpdateCustomerRequest request)
        {
            return new UpdateCustomerResponse
            {
                Error = _customerService.UpdateCustomer(id, request)
            };
        }

        [HttpDelete("{id}")]
        public DeleteCustomerResponse DeleteCustomer(int id)
        {
            return new DeleteCustomerResponse
            {
                Error = _customerService.DeleteCustomer(id)
            };
        }

        [HttpDelete("DeleteAll")]
        public DeleteCustomerResponse DeleteAllCustomers()
        {
            return new DeleteCustomerResponse
            {
                Error = _customerService.DeleteAllCustomers()
            };
        }

        [HttpGet("SearchByName/{searchTerm}")]
        public SearchCustomerResponse SearchByName(string searchTerm)
        {
            var result = _customerService.SearchByName(searchTerm);
            return new SearchCustomerResponse
            {
                Error = result.Error,
                Customers = result.Customers
            };
        }

        [HttpGet("SearchBySurname/{searchTerm}")]
        public SearchCustomerResponse SearchBySurname(string searchTerm)
        {
            var result = _customerService.SearchBySurname(searchTerm);
            return new SearchCustomerResponse
            {
                Error = result.Error,
                Customers = result.Customers
            };
        }

        [HttpGet("getBanned")]
        public GetCustomersResponse GetBannedCustomers()
        {
            var result = _customerService.GetBannedCustomers();
            return new GetCustomersResponse
            {
                Customers = result.Customers,
                Error = result.Error
            };
        }

        [HttpGet("getNotBanned")]
        public GetCustomersResponse GetNotBannedCustomers()
        {
            var result = _customerService.GetNotBannedCustomers();
            return new GetCustomersResponse
            {
                Customers = result.Customers,
                Error = result.Error
            };
        }
    }
}