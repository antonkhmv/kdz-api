﻿using Microsoft.AspNetCore.Mvc;

namespace HSEApiTraining.Controllers
{
    //      ПРЕЗЕНТЕЙШОН ЛЕУЕР
    [Route("api/[controller]")]
    [ApiController]
    public class DummyController : Controller
    {
        private readonly IDummyService _IDummyService;

        public DummyController(IDummyService IDummyService)
        {
            _IDummyService = IDummyService;
        }
        [HttpGet("generate/{number}")]
        public string DummyGenerator(int number)
        {
            return _IDummyService.DummyGenerator(number);
        }
    }
}
