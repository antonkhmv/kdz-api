﻿using HSEApiTraining.Models.Currency;
using Microsoft.AspNetCore.Mvc;

namespace HSEApiTraining.Controllers
{
    //      ПРЕЗЕНТЕЙШОН ЛЕУЕР
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : Controller
    {
        private readonly ICurrencyService _CurrecnyService;
        public CurrencyController(ICurrencyService currecnyService)
        {
            _CurrecnyService = currecnyService;
        }

        [HttpPost]
        public CurrecnyResponse GetCurrecnyResponse([FromBody] CurrencyRequest request)
        {
            return _CurrecnyService.GetCurrencyResponse(request).Result;
        }
    }
}
