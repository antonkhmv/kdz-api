﻿using HSEApiTraining.Models.Calculator;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HSEApiTraining.Controllers
{
    //      ПРЕЗЕНТЕЙШОН ЛЕУЕР
    [Route("api/[controller]")]
    [ApiController]
    public class CalculatorController : ControllerBase
    {
        private readonly ICalculatorService _calculatorService;
        //В конструкторе контроллера происходит инъекция сервисов через их интерфейсы
        public CalculatorController(ICalculatorService calculatorService)
        {
            _calculatorService = calculatorService;
        }

        [HttpGet]
        public CalculatorResponse Calculate([FromQuery] string expression)
        {
            //Тут нужно подключить реализованную в сервисе calculatorService логику вычисления выражений
            //В нижнем методе - аналогично
            string error = null;
            double result = 0;
            try
            {
                result = _calculatorService.CalculateExpression(expression);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            return new CalculatorResponse
            {
                Value = result,
                Error = error
            };
        }

        [HttpPost]
        public CalculatorBatchResponse CalculateBatch([FromBody] CalculatorBatchRequest Request)
        {
            string batchError = null;
            //Тут организуйте подсчет и формирование ответа сами
            IEnumerable<CalculatorResponse> results = new List<CalculatorResponse>();
            try
            {
                results = _calculatorService.CalculateBatchExpressions(Request.Expressions)
                    .Select(x => new CalculatorResponse() { Value = x });
            }
            catch (Exception ex)
            {
                batchError = ex.Message;
            }
            return new CalculatorBatchResponse
            {
                Values = results,
                Error = batchError
            };
        }

        //Примеры-пустышки
        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

        }
    }
}
