﻿using Dapper;
using HSEApiTraining.Ban;
using HSEApiTraining.Providers;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

namespace HSEApiTraining.Repositories
{
    //  ДАТА - АКССЕСС ЛЕЙУЕР
    public interface IBanRepository
    {
        (IEnumerable<Phone> Phones, string Error) GetAllPhones();
        string DeletePhone(int id);
        string DeleteAllPhones();
        string AddPhone(string number);
    }
    public class BanRepository : IBanRepository
    {
        private readonly ISQLiteConnectionProvider _connectionProvider;

        public BanRepository(ISQLiteConnectionProvider sqliteConnectionProvider)
        {
            _connectionProvider = sqliteConnectionProvider;
        }

        /// <summary>
        /// Проверка на наличие значения value в дб из connection
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        static bool CheckForMatches(SQLiteConnection connection, string value)
        {
            return connection.Query<string>(@"
                SELECT  
                    phone
                FROM banned_phone
                WHERE (phone = @Value);", new { Value = value })
            .Count() != 0;
        }

        public string AddPhone(string phone)
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();

                    if (CheckForMatches(connection, phone))
                    {
                        return "Уже забанил, успокойся.";
                    }

                    connection.Execute(
                        @"INSERT INTO 
                            banned_phone(phone)
                        VALUES (@phone);", new { phone = phone });
                }
                return null;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string DeleteAllPhones()
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();
                    connection.Execute(
                        @"DELETE FROM banned_phone");
                }
                return null;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Смотрит есть ли айди в бд
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private static bool CheckId(SQLiteConnection connection, int id)
        {
            if (connection.Query<int>(@"
                    SELECT 
                        id
                    FROM 
                        banned_phone
                    WHERE (id = @Id);
                    ", new { Id = id })
                .Count() == 0)
            {
                return false;
            }
            return true;
        }

        public string DeletePhone(int id)
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();

                    if (!CheckId(connection, id))
                    {
                        return ("Ты что дуак?? Нет такого айди, не буду удалять телефон ‎凸(ಠ益ಠ)凸.");
                    }

                    connection.Execute(
                        @"DELETE FROM banned_phone WHERE id = @Id;",
                        new { Id = id });
                }
                return null;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public (IEnumerable<Phone> Phones, string Error) GetAllPhones()
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();
                    return (
                    connection.Query<Phone>(
                        @"SELECT 
                            id as Id, 
                            phone as PhoneNumber
                        FROM banned_phone"
                        ),
                  null);
                }
            }
            catch (Exception e)
            {
                return (null, e.Message);
            }
        }
    }
}