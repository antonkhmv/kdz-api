﻿using Dapper;
using HSEApiTraining.Models.Customer;
using HSEApiTraining.Providers;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text.RegularExpressions;

namespace HSEApiTraining
{
    //  ДАТА - АКССЕСС ЛЕЙУЕР
    public interface ICustomerRepository
    {
        (IEnumerable<Customer> Customers, string Error) GetCustomers(int count);
        (IEnumerable<Customer> Customers, string Error) GetAllCustomers();
        (IEnumerable<Customer> Customers, string Error) GetBannedCustomers();
        (IEnumerable<Customer> Customers, string Error) GetNotBannedCustomers();
        string AddCustomer(AddCustomerRequest request);
        string DeleteCustomer(int id);
        string UpdateCustomer(int id, UpdateCustomerRequest request);
        string GenerateRandomCustomers(List<AddCustomerRequest> customers);
        string DeleteAllCustomers();
        (IEnumerable<Customer> Customers, string Error) SearchBySurname(string searchTerm);
        (IEnumerable<Customer> Customers, string Error) SearchByName(string searchTerm);
    }

    public class CustomerRepository : ICustomerRepository
    {
        private readonly ISQLiteConnectionProvider _connectionProvider;
        public CustomerRepository(ISQLiteConnectionProvider sqliteConnectionProvider)
        {
            _connectionProvider = sqliteConnectionProvider;
        }

        public (IEnumerable<Customer> Customers, string Error) GetCustomers(int count)
        {
            // Раньшше еррор-месседжи были на Енглиш, теперь на Рашшн,, хз почему
            if (count < 0)
            {
                return (null, "Ты шо дурак? Зачем сюда число меньше 0 написал?????");
            }

            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();
                    return (
                        connection.Query<Customer>(@"
                        SELECT 
                        id as Id,
                        name as Name, 
                        surname as Surname, 
                        phone_number as PhoneNumber 
                        FROM Customer 
                        LIMIT @count",
                        new { count = count }),
                        null);
                }
            }
            catch (Exception e)
            {
                return (null, e.Message);
            }
        }

        public (IEnumerable<Customer> Customers, string Error) GetAllCustomers()
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();
                    return (
                        connection.Query<Customer>(@"
                        SELECT 
                            id as Id,
                            name as Name, 
                            surname as Surname, 
                            phone_number as PhoneNumber 
                        FROM Customer"),
                        null);
                }
            }
            catch (Exception e)
            {
                return (null, e.Message);
            }
        }

        public string AddCustomer(AddCustomerRequest request)
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();
                    connection.Execute(
                        @"INSERT INTO Customer 
                        ( name, surname, phone_number ) VALUES 
                        ( @Name, @Surname, @PhoneNumber );",
                        new { Name = request.Name, Surname = request.Surname, PhoneNumber = request.PhoneNumber });
                }
                return null;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Проверяет наличие айди в бд
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool CheckId(SQLiteConnection connection, int id)
        {
            if (connection.Query<int>(@"
                    SELECT id FROM Customer
                    WHERE id = @Id
                    ", new { Id = id })
                .Count() == 0)
            {
                return false;
            }
            return true;
        }

        public string DeleteCustomer(int id)
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();

                    if (!CheckId(connection, id))
                    {
                        return ("Ты что дурак?? Нет такого айди, не буду удалять кастомера ‎凸(ಠ益ಠ)凸.");
                    }

                    connection.Execute(
                        @"DELETE FROM Customer WHERE id = @Id;",
                        new { Id = id });
                }
                return null;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string UpdateCustomer(int id, UpdateCustomerRequest request)
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();

                    if (!CheckId(connection, id))
                    {
                        return ("Ты что дурак?? Нет такого айди, не буду обновлять кастомера ‎凸(ಠ益ಠ)凸.");
                    }
                    connection.Execute(
                        @"UPDATE Customer 
                        SET name = @Name, surname = @Surname, phone_number = @PhoneNumber
                        WHERE id = @Id;",
                        new { Name = request.Name, Surname = request.Surname, PhoneNumber = request.PhoneNumber, Id = id });
                }
                return null;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Генерим кастомеров в слое бизнесс-логики и пушим их в дб в слое дата-акссесс
        /// </summary>
        /// <param name="customers"></param>
        /// <returns></returns>
        public string GenerateRandomCustomers(List<AddCustomerRequest> customers)
        {
            try
            {
                // Тут я чета делаю из стаковревлоув, вроде так быстрее
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();

                    using (var command = new SQLiteCommand(connection))
                    {
                        using (var transactioin = connection.BeginTransaction())
                        {
                            foreach (var customer in customers)
                            {
                                command.CommandText = @"
                                    INSERT INTO 
                                        Customer(name, surname, phone_number)
                                    VALUES
                                        (@Name, @Surname, @PhoneNumber)";

                                command.Parameters.AddWithValue("Name", customer.Name);
                                command.Parameters.AddWithValue("Surname", customer.Surname);
                                command.Parameters.AddWithValue("PhoneNumber", customer.PhoneNumber);

                                command.ExecuteNonQuery();
                            }
                            transactioin.Commit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return null;
        }

        public string DeleteAllCustomers()
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();
                    connection.Execute(@"DELETE FROM Customer");
                }
                return null;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public (IEnumerable<Customer> Customers, string Error) SearchBySurname(string searchTerm)
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();
                    return (
                        connection.Query<Customer>(@"
                        SELECT 
                            id as Id,
                            name as Name, 
                            surname as Surname, 
                            phone_number as PhoneNumber 
                        FROM 
                            Customer 
                        WHERE surname LIKE @SearchTerm",
                        new { SearchTerm = "%" +
                        // ВО ТАК сказали на стаковерфлоув делать, шобы
                        // кулхацкеры не забрали ваши пароли
                            Regex.Replace(searchTerm, @"([%_\[])", @"[$1]")
                        + "%" }), null);
                        // А процентики для подстрок
                }
            }
            catch (Exception e)
            {
                return (null, e.Message);
            }
        }

        public (IEnumerable<Customer> Customers, string Error) SearchByName(string searchTerm)
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();
                    return (
                        connection.Query<Customer>(@"
                        SELECT 
                            id as Id,
                            name as Name, 
                            surname as Surname, 
                            phone_number as PhoneNumber 
                        FROM 
                            Customer 
                        WHERE name LIKE @SearchTerm",
                        new
                        {
                            SearchTerm = "%" +
                            Regex.Replace(searchTerm, @"([%_\[])", @"[$1]")
                        + "%"
                        }), null);
                }
            }
            catch (Exception e)
            {
                return (null, e.Message);
            }
        }

        public (IEnumerable<Customer> Customers, string Error) GetBannedCustomers()
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();
                    return (
                        connection.Query<Customer>(@"
                        SELECT 
                            id as Id,
                            name as Name, 
                            surname as Surname, 
                            phone_number as PhoneNumber 
                        FROM Customer
                        WHERE phone_number IN (
                            SELECT
                                phone
                            FROM 
                                banned_phone
                        )"),
                        null);
                }
            }
            catch (Exception e)
            {
                return (null, e.Message);
            }
        }

        public (IEnumerable<Customer> Customers, string Error) GetNotBannedCustomers()
        {
            try
            {
                using (var connection = _connectionProvider.GetDbConnection())
                {
                    connection.Open();
                    return (
                        connection.Query<Customer>(@"
                        SELECT 
                            id as Id,
                            name as Name, 
                            surname as Surname, 
                            phone_number as PhoneNumber 
                        FROM Customer
                        WHERE phone_number NOT IN (
                            SELECT
                                phone
                            FROM 
                                banned_phone
                        )"),
                        null);
                }
            }
            catch (Exception e)
            {
                return (null, e.Message);
            }
        }
    }
}
