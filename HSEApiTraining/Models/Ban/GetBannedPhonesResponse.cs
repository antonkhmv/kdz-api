﻿using HSEApiTraining.Ban;
using System.Collections.Generic;

namespace HSEApiTraining.Models.Ban
{
    public class GetBannedPhonesResponse
    {
        public IEnumerable<Phone> Phones{ get; set; } 
        public string Error { get; set; }
    }
}