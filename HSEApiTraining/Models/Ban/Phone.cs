﻿namespace HSEApiTraining.Ban {
    public class Phone
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
    }
}