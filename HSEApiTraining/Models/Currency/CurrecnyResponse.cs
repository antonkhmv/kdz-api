﻿using System.Collections.Generic;

namespace HSEApiTraining.Models.Currency
{
    public class CurrecnyResponse
    {
        public List<double> CurrencyRate { get; set; } = new List<double>(); // стоимость валюты в рублях за каждый запрошенный день

        public string Error { get; set; } 
    }
}
