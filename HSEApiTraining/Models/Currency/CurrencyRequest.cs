﻿using System;

namespace HSEApiTraining.Models.Currency
{
    public class CurrencyRequest
    {
        public string Symbol { get; set; }// трехбуквенное обозначение целевой валюты

        public DateTime? DateStart { get; set; }// необязательный аргумент, 
                                                //если он null - использовать текущую дату

        public DateTime? DateEnd { get; set; } //необязательный аргумент, если он null, считать,
                                               //что нужно обработать только промежуток c DateStart 
                                               //по DateStart, то есть только один день

    }
}
