﻿using System.Collections.Generic;

namespace HSEApiTraining.Models.Calculator
{
    public class CalculatorBatchResponse
    {
        public IEnumerable<CalculatorResponse> Values { get; set; }
        public string Error { get; set; }
    }
}
