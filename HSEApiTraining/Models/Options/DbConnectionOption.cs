﻿namespace HSEApiTraining.Models.Options
{
    public class DbConnectionOptions
    {
        public string ConnectionString { get; set; }
    }
}